/** @type {import('tailwindcss').Config} */
module.exports = {
	content: [
		'node_modules/daisyui/dist/**/*.js',
		'node_modules/react-daisyui/dist/**/*.js',
		'./src/pages/**/*.{js,ts,jsx,tsx}',
		'./src/components/**/*.{js,ts,jsx,tsx}',
		'./src/slices/**/*.{js,ts,jsx,tsx}',
	],
	theme: {},
	plugins: [require('daisyui')],
};
