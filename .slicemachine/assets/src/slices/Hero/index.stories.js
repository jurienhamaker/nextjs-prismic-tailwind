import MyComponent from '../../../../../src/slices/Hero';

export default {
	title: 'src/slices/Hero',
};

export const _Default = () => (
	<MyComponent
		slice={{
			variation: 'default',
			version: 'sktwi1xtmkfgx8626',
			items: [{}],
			primary: {
				title: [{ type: 'heading1', text: 'Finish', spans: [] }],
				description: [
					{
						type: 'paragraph',
						text: 'Ad cillum aliqua magna cupidatat eiusmod aute commodo sunt laborum ut. Commodo anim esse reprehenderit mollit nostrud.',
						spans: [],
					},
				],
				button_text: 'thick',
				button_link: { link_type: 'Web', url: 'https://prismic.io' },
			},
			slice_type: 'hero',
			id: '_Default',
		}}
	/>
);
_Default.storyName = '';
