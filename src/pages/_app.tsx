import { PrismicPreview } from '@prismicio/next';
import { PrismicLink, PrismicProvider } from '@prismicio/react';
import Link from 'next/link';
import { Theme, useTheme } from 'react-daisyui';

import { linkResolver, repositoryName } from '../../prismicio';

import '../styles/globals.scss';

const NextLinkShim = ({ href, locale, children, ...props }) => {
	return (
		<Link href={href} locale={locale}>
			<a {...props}>{children}</a>
		</Link>
	);
};

/** @type {import('@prismicio/react').JSXMapSerializer} */
const richTextComponents = {
	hyperlink: ({ children, node }) => (
		<PrismicLink
			field={node.data}
			className="underline decoration-1 underline-offset-1"
		>
			{children}
		</PrismicLink>
	),
};

function App({ Component, pageProps }) {
	const { theme } = useTheme();

	return (
		<PrismicProvider
			linkResolver={linkResolver}
			internalLinkComponent={NextLinkShim}
			richTextComponents={richTextComponents}
		>
			<PrismicPreview repositoryName={repositoryName}>
				<Theme dataTheme={theme} className="h-screen w-full">
					<Component {...pageProps} />
				</Theme>
			</PrismicPreview>
		</PrismicProvider>
	);
}

export default App;
