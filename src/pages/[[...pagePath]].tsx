import * as prismicH from '@prismicio/helpers';
import { SliceZone } from '@prismicio/react';
import Head from 'next/head';

import { createClient, linkResolver } from '../../prismicio';
import { components } from '../slices';

import { Layout } from '../components/Layout';

const Page = ({
	locale,
	settings,
	mainNavigation,
	page: {
		data: { meta_title, meta_description, slices },
	},
}) => {
	return (
		<Layout mainNavigation={mainNavigation} locale={locale}>
			<Head>
				<title>
					{meta_title} | {settings.name}
				</title>
				<meta name="description" content={meta_description} />
			</Head>
			<SliceZone slices={slices} components={components} />
		</Layout>
	);
};

export default Page;

export async function getStaticProps({ params, previewData, locale }) {
	const client = createClient({ previewData });

	const uid = params.path?.[params.path.length - 1] || 'home';
	const page = await client.getByUID('page', uid, { lang: locale });
	const { data: mainNavigation } = await client.getSingle('main_navigation', {
		lang: locale,
	});
	const { data: settings } = await client.getSingle('settings', {
		lang: locale,
	});

	return {
		props: {
			locale,
			page,
			mainNavigation,
			settings,
		},
	};
}

export async function getStaticPaths() {
	const client = createClient();

	const pages = await client.getAllByType('page', { lang: '*' });

	return {
		paths: pages.map((page) => prismicH.asLink(page, linkResolver)),
		fallback: false,
	};
}
