import { PrismicRichText } from '@prismicio/react';
import Link from 'next/link';
import { Button, Hero as DHero } from 'react-daisyui';

const Hero = ({ slice }) => (
	<DHero>
		<DHero.Overlay className="bg-opacity-60" />
		<DHero.Content className="text-center">
			<div className="max-w-md">
				<h1 className="text-5xl font-bold">
					<PrismicRichText field={slice.primary.title} />
				</h1>
				<p className="py-6">
					<PrismicRichText field={slice.primary.description} />
				</p>

				{slice.primary.button_text && slice.primary.button_link && (
					<Link href={slice.primary.button_link.url}>
						<Button color="primary">
							{slice.primary.button_text}
						</Button>
					</Link>
				)}
			</div>
		</DHero.Content>
	</DHero>
);

export default Hero;
