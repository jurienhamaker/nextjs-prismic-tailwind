import { useEffect, useState } from 'react';

import { DEFAULT_THEME, THEME_STORAGE_KEY } from '../utils/consts';

export const useGlobalTheme = () => {
	if (typeof window === 'undefined') {
		return DEFAULT_THEME;
	}

	// eslint-disable-next-line react-hooks/rules-of-hooks
	const [theme, setTheme] = useState(
		window.localStorage.getItem(THEME_STORAGE_KEY) || DEFAULT_THEME
	);

	// eslint-disable-next-line react-hooks/rules-of-hooks
	useEffect(() => {
		const handleLocalStorageChange = () => {
			let storageTheme = window.localStorage.getItem(THEME_STORAGE_KEY);

			if (!storageTheme) {
				setTheme(DEFAULT_THEME);
				return;
			}

			setTheme(storageTheme);
		};

		// Add listener
		window.addEventListener('storage', handleLocalStorageChange);

		// Destory listener when unmounting
		return () => {
			window.removeEventListener('storage', handleLocalStorageChange);
		};
	}, []);

	return theme;
};
