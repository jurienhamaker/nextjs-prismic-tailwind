import { Footer } from './Footer';
import { Navigation } from './Navbar';

export const Layout = ({ mainNavigation, locale, children }) => {
	return (
		<>
			<Navigation mainNavigation={mainNavigation} locale={locale} />

			<main>{children}</main>

			<Footer locale={locale} />
		</>
	);
};
