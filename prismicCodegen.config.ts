import type { Config } from 'prismic-ts-codegen';

const config: Config = {
	output: './types.generated.ts',

	models: ['./src/customtypes/**/index.json', './src/slices/**/model.json'],
};

export default config;
